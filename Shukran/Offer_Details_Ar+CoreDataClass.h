//
//  Offer_Details_Ar+CoreDataClass.h
//  
//
//  Created by Venkatesh Badya on 29/09/16.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface Offer_Details_Ar : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "Offer_Details_Ar+CoreDataProperties.h"
