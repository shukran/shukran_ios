//
//  DatabaseUtility.h
//  Infinea
//
//  Created by Venkatesh on 5/6/15.
//  Copyright (c) 2015 Ritu Dalal. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface DatabaseUtility : NSObject
{
    NSManagedObjectContext *managedObjectContext;
    NSManagedObject *managedObject;
}
@property NSManagedObjectContext *_managedObjectContext;
@property NSManagedObject *_managedObject;
+(DatabaseUtility *) getUtility;
-(NSEntityDescription *) getEntityDescription:(NSString *)tableName;
-(BOOL) isDataAvailableInTable:(NSString *) tableName forPredicate:(NSPredicate *)predicate;
-(NSArray *) getAllDataFromTable:(NSString *) tableName forPredicate:(NSPredicate *) predicate withDescriptor:(NSArray *)sortDescriptors;
-(BOOL) deleteRecordsFromTable:(NSString *)tableName withRecord:(NSManagedObject *)managedObject;
+(BOOL) isBlank:(NSString *)data;
+(void) alertViewTitle:(NSString *)title withMessage:(NSString *)message;
+(BOOL) IsValidEmail:(NSString *)emailString Strict:(BOOL)strictFilter;
-(BOOL)insertDataInTable:(NSString *)tableName withDictionary:(NSMutableDictionary *)offerDictionary;
@end
