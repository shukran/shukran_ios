//
//  ServiceClass.m
//  Shukran
//
//  Created by Venkatesh Badya on 24/09/16.
//  Copyright © 2016 The Lean Apps. All rights reserved.
//

#import "ServiceClass.h"
#import "AFNetworking.h"
#import "Define.h"

@implementation ServiceClass

// Making Singolton class
+(ServiceClass *) sharedServiceClass
{
    static ServiceClass *singolton;
    if(!singolton) {
        singolton=[[ServiceClass alloc] init];
    }
    return singolton;
}

#pragma mark Service

// Fetching Offer List
-(void)offerServiceList:(NSMutableDictionary *)params {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager]; // Creating AFHTTPRequestOperationManager object
    manager.securityPolicy.allowInvalidCertificates = YES; // Allowing invalid certificates
    [manager setResponseSerializer:[AFHTTPResponseSerializer serializer]]; //Setting response serializer
    manager.requestSerializer.timeoutInterval = 5; // Setting time out interval
    [manager.requestSerializer setValue:@"@#$%ddddddd@#$%^&!@##43434324234324@#" forHTTPHeaderField:@"shukran-api-key"]; // Setting service header
    [manager GET:[NSString stringWithFormat:@"%@offer.json",BASEURL] parameters:params success: ^(AFHTTPRequestOperation *operation, id responseObject) {
         id parsedData   = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil]; // Parsing response
         [[NSNotificationCenter defaultCenter] postNotificationName: Notification_Offer_List object:nil userInfo:parsedData]; // Posting notification
     }
          failure: ^(AFHTTPRequestOperation *operation, NSError *error) {
          [[NSNotificationCenter defaultCenter] postNotificationName: Notification_Offer_List object:nil userInfo:nil]; // Posting notification
     }];
}

@end
