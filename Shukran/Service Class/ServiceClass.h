//
//  ServiceClass.h
//  Shukran
//
//  Created by Venkatesh Badya on 24/09/16.
//  Copyright © 2016 The Lean Apps. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ServiceClass : NSObject

-(void)offerServiceList:(NSMutableDictionary *)params;
+(ServiceClass *) sharedServiceClass;

@end
