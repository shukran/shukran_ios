//
//  Offer_Details_En+CoreDataProperties.h
//  
//
//  Created by Venkatesh Badya on 29/09/16.
//
//

#import "Offer_Details_En+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Offer_Details_En (CoreDataProperties)

+ (NSFetchRequest<Offer_Details_En *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *brandImage_en;
@property (nullable, nonatomic, copy) NSString *imageName_en;
@property (nullable, nonatomic, copy) NSString *languageCode_en;
@property (nullable, nonatomic, copy) NSString *longDescription_en;
@property (nullable, nonatomic, copy) NSString *offerId_en;
@property (nullable, nonatomic, copy) NSString *offerThumbImage_en;
@property (nullable, nonatomic, copy) NSString *offerTitle_en;
@property (nullable, nonatomic, copy) NSString *offerValidDate_en;
@property (nullable, nonatomic, copy) NSString *shortDescription_en;
@property (nullable, nonatomic, copy) NSString *offerCreatedDate_en;
@property (nullable, nonatomic, copy) NSString *offerPrivilege;

@end

NS_ASSUME_NONNULL_END
