//
//  Offer_Details_En+CoreDataProperties.m
//  
//
//  Created by Venkatesh Badya on 29/09/16.
//
//

#import "Offer_Details_En+CoreDataProperties.h"

@implementation Offer_Details_En (CoreDataProperties)

+ (NSFetchRequest<Offer_Details_En *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Offer_Details_En"];
}

@dynamic brandImage_en;
@dynamic imageName_en;
@dynamic languageCode_en;
@dynamic longDescription_en;
@dynamic offerId_en;
@dynamic offerThumbImage_en;
@dynamic offerTitle_en;
@dynamic offerValidDate_en;
@dynamic shortDescription_en;
@dynamic offerCreatedDate_en;
@dynamic offerPrivilege;

@end
