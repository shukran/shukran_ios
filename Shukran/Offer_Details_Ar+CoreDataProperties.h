//
//  Offer_Details_Ar+CoreDataProperties.h
//  
//
//  Created by Venkatesh Badya on 29/09/16.
//
//

#import "Offer_Details_Ar+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Offer_Details_Ar (CoreDataProperties)

+ (NSFetchRequest<Offer_Details_Ar *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *brandImage_ar;
@property (nullable, nonatomic, copy) NSString *imageName_ar;
@property (nullable, nonatomic, copy) NSString *languageCode_ar;
@property (nullable, nonatomic, copy) NSString *longDescription_ar;
@property (nullable, nonatomic, copy) NSString *offerId_ar;
@property (nullable, nonatomic, copy) NSString *offerThumbImage_ar;
@property (nullable, nonatomic, copy) NSString *offerTitle_ar;
@property (nullable, nonatomic, copy) NSString *offerValidDate_ar;
@property (nullable, nonatomic, copy) NSString *shortDescription_ar;
@property (nullable, nonatomic, copy) NSString *offerCreatedDate_ar;
@property (nullable, nonatomic, copy) NSString *offerPrivilege;

@end

NS_ASSUME_NONNULL_END
