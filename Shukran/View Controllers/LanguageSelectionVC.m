//
//  LanguageSelectionVC.m
//  Shukran
//
//  Created by Venkatesh Badya on 26/09/16.
//  Copyright © 2016 The Lean Apps. All rights reserved.
//

#import "LanguageSelectionVC.h"
#import "AppHelper.h"
#import "AppDelegate.h"

@interface LanguageSelectionVC ()
{
    // Object for UIStoryboard
    UIStoryboard *storyBoard;
}
//Outlets for Label
@property (weak, nonatomic) IBOutlet UILabel *headingLabel;

@end

@implementation LanguageSelectionVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.headingLabel.text = NSLocalizedStringFromTable(@"Choose your Language", [AppHelper userDefaultsForKey:@"Language"], nil);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Button Actions

//English Language Button Action
- (IBAction)englishLanguageButtonAction:(id)sender {
    [AppHelper saveToUserDefaults:@"English" withKey:@"Language"];
    [self navigateToOfferScreen];
}

//Arabic Language Button Action
- (IBAction)arabicLanguageButtonAction:(id)sender {
    [AppHelper saveToUserDefaults:@"Arabic" withKey:@"Language"];
    [self navigateToOfferScreen];
}

// Navigation to Offer List screen
-(void)navigateToOfferScreen
{
    [AppHelper saveToUserDefaults:@"Yes" withKey:@"LanguageSelected"];
    UIViewController * initialViewController = [[UIStoryboard storyboardWithName:@"StoryboardEnglish" bundle:nil] instantiateViewControllerWithIdentifier:@"OfferScreenVC"];
    CATransition* transition = [CATransition animation];
    transition.duration = 0.3;
    transition.type = kCATransitionPush;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    if([[AppHelper userDefaultsForKey:@"Language"] isEqualToString:@"English"]) {
        transition.subtype = kCATransitionFromRight;
    }
    else {
        transition.subtype = kCATransitionFromLeft;
    }
    [self.navigationController.view.layer addAnimation:transition forKey:nil];
    [self.navigationController pushViewController:initialViewController animated:NO];
}

@end
