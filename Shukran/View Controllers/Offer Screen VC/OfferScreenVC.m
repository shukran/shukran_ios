//
//  OfferScreenVC.m
//  Shukran
//
//  Created by Venkatesh Badya on 22/09/16.
//  Copyright © 2016 The Lean Apps. All rights reserved.
//

#import "OfferScreenVC.h"
#import "OfferDetailVC.h"
#import "Reachability.h"
#import "ServiceClass.h"
#import "Define.h"
#import "UIView+Toast.h"
#import "SVPullToRefresh.h"
#import "DatabaseUtility.h"
#import <CoreData/CoreData.h>
#import "AppDelegate.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "AppHelper.h"

@interface OfferScreenVC ()<UITableViewDelegate, UITableViewDataSource>
{
    // Objects for NSIndexPath, Reachability and NSDateFormatter
    NSIndexPath * selectedIndexPath;
    Reachability * networkReachability;
    NSDateFormatter *dateFormatter;
    
    // To show pagination if exists
    BOOL loadMore;
    NSInteger currentPageCount;
    NSInteger totalPageCount;
    
    // Creating Dictionary and MutableArray to store the details
    NSDictionary * offerListDictionary;
    NSMutableArray * offerListArray;
}

//Outlets for TableView, UIButton and View
@property (weak, nonatomic) IBOutlet UITableView *offerTableView;
@property (weak, nonatomic) IBOutlet UIView *networkConnectionView;
@property (weak, nonatomic) IBOutlet UIButton *languageButton;
@property (weak, nonatomic) IBOutlet UILabel *noInternetConnectionLabel;

//Outlets for ActivityIndicatorView and ActivityIndicator
@property (weak, nonatomic) IBOutlet UIView *activityIndicatorView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@end

@implementation OfferScreenVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    currentPageCount = 1;
    //Checking for network connectivity
    networkReachability = [Reachability reachabilityForInternetConnection];
    [networkReachability startNotifier];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(checkNetworkStatus:)
                                                 name:kReachabilityChangedNotification object:nil];
    
    //Customizing ActivityIndicator
    self.activityIndicator.color = [UIColor whiteColor];
    
    //Checking for network connection and calling function to fetch offer list depending upon network connection availability
    networkReachability = [Reachability reachabilityForInternetConnection];
    if([networkReachability isReachable]) {
        offerListArray = [NSMutableArray array];
        self.networkConnectionView.hidden = YES;
        self.activityIndicatorView.hidden = NO;
        [self OfferListService:currentPageCount :NO];
    }
    else {
        // Fetching objects from database
        offerListArray = [NSMutableArray array];
        DatabaseUtility *ut = [DatabaseUtility getUtility];
        NSString *tableName;
        if([[AppHelper userDefaultsForKey:@"Language"] isEqualToString:@"English"]) {
            tableName = @"Offer_Details_En";
        }
        else {
            tableName = @"Offer_Details_Ar";
        }
        NSArray * offerObjects = [ut getAllDataFromTable:tableName forPredicate:nil withDescriptor:nil];
        for (int offerCounter = 0; offerCounter < offerObjects.count ; offerCounter ++) {
            NSManagedObject * managedObject = [offerObjects objectAtIndex:offerCounter];
            
            NSMutableDictionary * offerDictionary = [NSMutableDictionary dictionary];
            
            if([[AppHelper userDefaultsForKey:@"Language"] isEqualToString:@"English"]) {
                [offerDictionary setValue:[managedObject valueForKey:@"brandImage_en"] forKey:@"BrandImage"];
                [offerDictionary setValue:[managedObject valueForKey:@"imageName_en"] forKey:@"ImageName"];
                [offerDictionary setValue:[managedObject valueForKey:@"languageCode_en"] forKey:@"LanguageCode"];
                [offerDictionary setValue:[managedObject valueForKey:@"longDescription_en"] forKey:@"LongDescription"];
                [offerDictionary setValue:[managedObject valueForKey:@"offerId_en"] forKey:@"OfferId"];
                [offerDictionary setValue:[managedObject valueForKey:@"offerThumbImage_en"] forKey:@"OfferThumbImage"];
                [offerDictionary setValue:[managedObject valueForKey:@"offerTitle_en"] forKey:@"OfferTitle"];
                [offerDictionary setValue:[managedObject valueForKey:@"offerValidDate_en"] forKey:@"OfferValidDate"];
                [offerDictionary setValue:[managedObject valueForKey:@"shortDescription_en"] forKey:@"SortDescription"];
                [offerDictionary setValue:[managedObject valueForKey:@"offerPrivilege"] forKey:@"Privilege"];
                [offerDictionary setValue:[managedObject valueForKey:@"offerCreatedDate_en"] forKey:@"Created"];
            }
            else {
                [offerDictionary setValue:[managedObject valueForKey:@"brandImage_ar"] forKey:@"BrandImage"];
                [offerDictionary setValue:[managedObject valueForKey:@"imageName_ar"] forKey:@"ImageName"];
                [offerDictionary setValue:[managedObject valueForKey:@"languageCode_ar"] forKey:@"LanguageCode"];
                [offerDictionary setValue:[managedObject valueForKey:@"longDescription_ar"] forKey:@"LongDescription"];
                [offerDictionary setValue:[managedObject valueForKey:@"offerId_ar"] forKey:@"OfferId"];
                [offerDictionary setValue:[managedObject valueForKey:@"offerThumbImage_ar"] forKey:@"OfferThumbImage"];
                [offerDictionary setValue:[managedObject valueForKey:@"offerTitle_ar"] forKey:@"OfferTitle"];
                [offerDictionary setValue:[managedObject valueForKey:@"offerValidDate_ar"] forKey:@"OfferValidDate"];
                [offerDictionary setValue:[managedObject valueForKey:@"shortDescription_ar"] forKey:@"SortDescription"];
                [offerDictionary setValue:[managedObject valueForKey:@"offerPrivilege"] forKey:@"Privilege"];
                [offerDictionary setValue:[managedObject valueForKey:@"offerCreatedDate_ar"] forKey:@"Created"];
            }
            
            [offerListArray addObject:offerDictionary];
        }
    }
    
    // Setting language button
    if([[AppHelper userDefaultsForKey:@"Language"] isEqualToString:@"English"]){
        [self.languageButton setImage:[UIImage imageNamed:@"Language Toogle Arabic"] forState:UIControlStateNormal];
        [self.languageButton setImage:[UIImage imageNamed:@"Language Toogle Arabic"] forState:UIControlStateHighlighted];
    }
    else {
        [self.languageButton setImage:[UIImage imageNamed:@"Language Toogle English"] forState:UIControlStateNormal];
        [self.languageButton setImage:[UIImage imageNamed:@"Language Toogle English"] forState:UIControlStateHighlighted];
    }
    
    self.noInternetConnectionLabel.text = NSLocalizedStringFromTable(@"No internet connection", [AppHelper userDefaultsForKey:@"Language"], nil);
}

-(void)viewWillAppear:(BOOL)animated {
    // Pull to refresh methods
    [self.offerTableView addPullToRefreshWithActionHandler:
     ^{
         //Checking for network connection and calling function to fetch offer list depending upon network connection availability
         networkReachability = [Reachability reachabilityForInternetConnection];
         if([networkReachability isReachable]) {
             self.networkConnectionView.hidden = YES;
             currentPageCount = 1;
             self.activityIndicatorView.hidden = NO;
             [self OfferListService:currentPageCount :YES];
         }
         else {
             self.networkConnectionView.hidden = NO;
             [self.offerTableView.pullToRefreshView stopAnimating];
         }
     }];
    
    // Setting RTL or LTR according to language
    if([[AppHelper userDefaultsForKey:@"Language"] isEqualToString:@"English"]){
        [[UIView appearance] setSemanticContentAttribute:UISemanticContentAttributeForceLeftToRight];
    }
    else{
        [[UIView appearance] setSemanticContentAttribute:UISemanticContentAttributeForceRightToLeft];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - StatusBar

//Changing StatusBar color
- (UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

#pragma mark - UITableView Delegates and DataSources

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return offerListArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell * offerCell;
    if(offerCell == nil) {
        offerCell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"OfferCell" forIndexPath:indexPath];
    }
    
    //Setting layer of ContainerView to hold Images and ValidDate
    UIView * containerView = (UIView *)[offerCell.contentView viewWithTag:100];
    containerView.layer.cornerRadius = 3.0;
    containerView.layer.borderWidth = 1.0;
    containerView.layer.masksToBounds = YES;
    containerView.layer.borderColor = [UIColor colorWithRed:230.0/255.0 green:230.0/255.0 blue:230.0/255.0 alpha:1.0].CGColor;
    
    // Fetching ImageView from tag and setting OfferImage to it
    __weak UIImageView *offerImage = (UIImageView *)[offerCell.contentView viewWithTag:101];
    NSURL * offerImageUrl;
    if(![[[offerListArray objectAtIndex:indexPath.row] objectForKey:@"ImageName"] isEqualToString:@""]) {
        offerImageUrl = [NSURL URLWithString:[[offerListArray objectAtIndex:indexPath.row] objectForKey:@"ImageName"]];
        [offerImage sd_setImageWithURL:offerImageUrl placeholderImage:[UIImage imageNamed:@"Offer Placeholder Image"]];
    }
    else {
        offerImageUrl = [NSURL URLWithString:@""];
        if([[AppHelper userDefaultsForKey:@"Language"] isEqualToString:@"Arabic"]) {
            offerImage.image = [UIImage imageNamed:@"No Offer Image Arabic"];
        }
        else {
            offerImage.image = [UIImage imageNamed:@"No Offer Image English"];
        }
    }
    
    // Fetching ImageView from tag and setting BrandImage to it
    __weak UIImageView *brandImage = (UIImageView *)[offerCell.contentView viewWithTag:104];
    NSURL * brandImageUrl;
    if(![[[offerListArray objectAtIndex:indexPath.row] objectForKey:@"BrandImage"]isEqualToString:@""]) {
        brandImageUrl = [NSURL URLWithString:[[offerListArray objectAtIndex:indexPath.row] objectForKey:@"BrandImage"]];
        [brandImage sd_setImageWithURL:brandImageUrl placeholderImage:[UIImage imageNamed:@"Brand Placeholder Image"]];
    }
    else {
        brandImageUrl = [NSURL URLWithString:@""];
        brandImage.image = [UIImage imageNamed:@"No Brand Image"];
    }
    
    // Fetching Button from tag and setting action to it
    UIButton * offerDescriptionButton = (UIButton *) [offerCell.contentView viewWithTag:102];
    [offerDescriptionButton addTarget:self action:@selector(navigateToOfferDetailScreen:) forControlEvents:UIControlEventTouchUpInside];

    //Setting text to ValidityLabel
    UILabel * offerValidityLabel = (UILabel *)[offerCell.contentView viewWithTag:103];
    if([[offerListArray objectAtIndex:indexPath.row] objectForKey:@"OfferValidDate"]) {
        
        dateFormatter = [[NSDateFormatter alloc] init];
        if([[AppHelper userDefaultsForKey:@"Language"] isEqualToString:@"Arabic"]) {
            [dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"ar"]];
        }
        else {
            [dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
        }
        [dateFormatter setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
        NSDate * offerDate = [dateFormatter dateFromString:[[offerListArray objectAtIndex:indexPath.row] objectForKey:@"OfferValidDate"]];
        
        // Convert date object into desired format
        [dateFormatter setDateFormat:@"dd MMM yyyy"];
        NSString * convertedDateFormat = [dateFormatter stringFromDate:offerDate];
        
        offerValidityLabel.text = [NSString stringWithFormat:@"%@ %@",NSLocalizedStringFromTable(@"Valid until:", [AppHelper userDefaultsForKey:@"Language"], nil),convertedDateFormat];
    }
    else {
        offerValidityLabel.text = @"";
    }
    
    //Setting text to OfferTitleLabel
    UILabel * offerTitleLabel = (UILabel *)[offerCell.contentView viewWithTag:105];
    if([[offerListArray objectAtIndex:indexPath.row] objectForKey:@"OfferTitle"]) {
        offerTitleLabel.text = [[[offerListArray objectAtIndex:indexPath.row] objectForKey:@"OfferTitle"] uppercaseString];
    }
    else {
        offerTitleLabel.text = @"";
    }
    
    //Checking for load more options if available for pagination
    if(loadMore && indexPath.row == [offerListArray count]-1) {
        networkReachability = [Reachability reachabilityForInternetConnection];
        if([networkReachability isReachable]) {
            self.networkConnectionView.hidden = YES;
            self.activityIndicatorView.hidden = NO;
            [self OfferListService:currentPageCount :NO];
        }
    }
    
    // If no pagination available
    if(!loadMore && indexPath.row == [offerListArray count]-1) {
        [self.navigationController.view makeToast:NSLocalizedStringFromTable(@"No more offers to display", [AppHelper userDefaultsForKey:@"Language"], nil)
                                         duration:2.0
                                         position:CSToastPositionBottom
                                            title:nil
                                            image:[UIImage imageNamed:@"toast.png"]
                                            style:nil
                                       completion:nil];
    }
    
    //Disabling cell selection style
    offerCell.selectionStyle = UITableViewCellSelectionStyleNone;

    return offerCell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return OFFERLISTCELLSIZE;
}

#pragma mark - Button Actions

// Navigation to Offer Detail Screen
-(void)navigateToOfferDetailScreen :(id) sender {
    OfferDetailVC *offerDetailObj = [self.storyboard instantiateViewControllerWithIdentifier:@"OfferDetailVC"];
    //Fetching selected IndexPath from sender
    UITableViewCell * offerCell = (UITableViewCell *)[[[sender superview] superview] superview];
    selectedIndexPath = [self.offerTableView indexPathForCell:offerCell];
    
    // Creating OfferDetailVC object and pushing to navigation stack
    offerDetailObj.offerDetailsDictionary = [offerListArray objectAtIndex:selectedIndexPath.row];
    
    if([[[offerListArray objectAtIndex:selectedIndexPath.row] objectForKey:@"Privilege"] isEqualToString:@"0"]) {
        offerDetailObj.latestOfferArray = [offerListArray mutableCopy];
        offerDetailObj.selectedIndex = selectedIndexPath;
        [self presentViewController:offerDetailObj animated:YES completion:nil];
    }
    else {
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Shukran" message:NSLocalizedStringFromTable(@"Unfortunately this offer is only available for Shukran members \n Please login to view the details and redeem the offer", [AppHelper userDefaultsForKey:@"Language"], nil) delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
}

// Cross Button Action
- (IBAction)crossButtonAction:(id)sender {
    self.networkConnectionView.hidden = YES;
}

//Language Selection Button
- (IBAction)languageButtonSelection:(id)sender{
    if([[AppHelper userDefaultsForKey:@"Language"] isEqualToString:@"Arabic"]) {
       [AppHelper saveToUserDefaults:@"English" withKey:@"Language"];
    }
    else if([[AppHelper userDefaultsForKey:@"Language"] isEqualToString:@"English"])  {
        [AppHelper saveToUserDefaults:@"Arabic" withKey:@"Language"];
    }
    [self changeStoryboardAccordingToLanguage];
}

#pragma mark - Changing Storyboard

//Changing Storyboard according to Language
-(void)changeStoryboardAccordingToLanguage
{
    //Changing Storyboard according to language selection
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"StoryboardEnglish" bundle:nil];
    UIViewController * initialViewController = [storyBoard instantiateViewControllerWithIdentifier:@"OfferScreenVC"];
    CATransition* transition = [CATransition animation];
    transition.duration = 0.3;
    transition.type = kCATransitionPush;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    if([[AppHelper userDefaultsForKey:@"Language"] isEqualToString:@"English"]) {
        transition.subtype = kCATransitionFromRight;
    }
    else {
        transition.subtype = kCATransitionFromLeft;
    }
    [self.navigationController.view.layer addAnimation:transition forKey:nil];
    [self.navigationController pushViewController:initialViewController animated:YES];
    
}

#pragma mark - Network Connectivity Functions

//Check for network connection
- (void)checkNetworkStatus:(NSNotification *)notice {
    // Called after network status changes
    NetworkStatus internetStatus = [networkReachability currentReachabilityStatus];
    switch (internetStatus)
    {
        case NotReachable: {
            self.networkConnectionView.hidden = NO;
            break;
        }
        case ReachableViaWiFi: {
            self.networkConnectionView.hidden = YES;
            loadMore = YES;
            currentPageCount = [[AppHelper userDefaultsForKey:@"PageCount"] integerValue]+1;
            break;
        }
        case ReachableViaWWAN: {
            self.networkConnectionView.hidden = YES;
            loadMore = YES;
            currentPageCount = [[AppHelper userDefaultsForKey:@"PageCount"] integerValue]+1;
            break;
        }
    }
}

#pragma mark - Web Service

//Calling web service to fetch offer list
-(void)OfferListService: (NSInteger)page :(BOOL)isFromRefresh {
    
    [AppHelper saveToUserDefaults:[NSString stringWithFormat:@"%ld",(long)page] withKey:@"PageCount"];
    NSMutableDictionary * offerListParams = [NSMutableDictionary dictionary];
    [offerListParams setValue:[NSString stringWithFormat:@"%ld",(long)page] forKey:@"Page"];
    if([[AppHelper userDefaultsForKey:@"Language"] isEqualToString:@"English"]) {
        [offerListParams setValue:@"en" forKey:@"LanguageCode"];
    }
    else {
        [offerListParams setValue:@"ar" forKey:@"LanguageCode"];
    }
    if(isFromRefresh) {
        if(offerListArray.count > 0) {
            [offerListParams setValue:[[offerListArray objectAtIndex:0] objectForKey:@"Created"] forKey:@"LastActiveOfferDate"];
        }
        else {
            [offerListParams setValue:@"" forKey:@"LastActiveOfferDate"];
        }
    }
    else {
        [offerListParams setValue:@"" forKey:@"LastActiveOfferDate"];
    }
    
    //Parameters - Page, LanguageCode and LastActiveOfferDate
    
    // Calling function to get OfferList and Stopping PullToRefresh Animation
    [self.offerTableView.pullToRefreshView stopAnimating];
    [[ServiceClass sharedServiceClass] offerServiceList:offerListParams];
    
    // Adding Observer of Product List
    __block __weak id gpsObserver=[[NSNotificationCenter defaultCenter] addObserverForName:Notification_Offer_List object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *notif) {
    [[NSNotificationCenter defaultCenter] removeObserver:gpsObserver name:Notification_Offer_List object:nil]; // Removing Notification of Product List
        if(notif.userInfo != nil) {
            offerListDictionary=notif.userInfo; // Getting parsed response
            if([[offerListDictionary objectForKey:@"code"] integerValue] == 204) {
                self.activityIndicatorView.hidden = YES;
                [self.navigationController.view makeToast:NSLocalizedStringFromTable(@"No offers to display", [AppHelper userDefaultsForKey:@"Language"], nil)
                                                 duration:0.4
                                                 position:CSToastPositionBottom
                                                    title:nil
                                                    image:[UIImage imageNamed:@"toast.png"]
                                                    style:nil
                                               completion:nil];
            }
            else {
                self.activityIndicatorView.hidden = YES;
                //Checking for PullToRefresh
                if(isFromRefresh) {
                    offerListArray = [NSMutableArray array];
                }
                if([[offerListDictionary objectForKey:@"data"] count] > 0) {
                    for(NSDictionary * offerDetailsDict in [offerListDictionary objectForKey:@"data"]) {
                        // Adding OfferListDictionary to OfferListArray
                        [offerListArray addObject:offerDetailsDict];
                    }
                    
                    //Deleting data from local database
                    DatabaseUtility *ut = [DatabaseUtility getUtility];
                    NSString *tableName;
                    if([[AppHelper userDefaultsForKey:@"Language"] isEqualToString:@"English"]) {
                        tableName = @"Offer_Details_En";
                    }
                    else {
                        tableName = @"Offer_Details_Ar";
                    }
                    
                    NSArray * offerObjects = [ut getAllDataFromTable:tableName forPredicate:nil withDescriptor:nil];
                    for (int offerCounter = 0; offerCounter < offerObjects.count ; offerCounter ++) {
                        NSManagedObject * managedObject = [offerObjects objectAtIndex:offerCounter];
                        [ut deleteRecordsFromTable:tableName withRecord:managedObject];
                    }
                    
                    //Inserting data into local database
                    for(NSMutableDictionary * offerDetailsDict in offerListArray) {
                        if([offerDetailsDict objectForKey:@"ImageName"]) {
                        }
                        [ut insertDataInTable:tableName withDictionary:offerDetailsDict];
                    }
                    [self.offerTableView reloadData]; // Reloading TableView
                    
                    if(isFromRefresh) {
                        if([[offerListDictionary objectForKey:@"TotalPullCount"] integerValue] == 0) {
                            [self.navigationController.view makeToast:NSLocalizedStringFromTable(@"No new offers found", [AppHelper userDefaultsForKey:@"Language"], nil)
                                                             duration:0.4
                                                             position:CSToastPositionBottom
                                                                title:nil
                                                                image:[UIImage imageNamed:@"toast.png"]
                                                                style:nil
                                                           completion:nil];
                        }
                        else {
                            [self.navigationController.view makeToast:[NSString stringWithFormat:@"%ld %@",[[offerListDictionary objectForKey:@"TotalPullCount"] integerValue],NSLocalizedStringFromTable(@"new offer(s) found", [AppHelper userDefaultsForKey:@"Language"], nil)]
                                                             duration:0.4
                                                             position:CSToastPositionBottom
                                                                title:nil
                                                                image:[UIImage imageNamed:@"toast.png"]
                                                                style:nil
                                                           completion:nil];
                        }
                    }
                }
            }
        }
        else {
            self.activityIndicatorView.hidden = YES;
            [self.navigationController.view makeToast:@"Please wait..."
                                             duration:2.0
                                             position:CSToastPositionBottom
                                                title:nil
                                                image:[UIImage imageNamed:@"toast.png"]
                                                style:nil
                                           completion:nil];
        }
        
        totalPageCount = [[[offerListDictionary objectForKey:@"pagination"] objectForKey:@"totalPages"] intValue];
        // Checking for pagination
        if(totalPageCount > currentPageCount) {
            loadMore=YES;
            currentPageCount = currentPageCount + 1;
        }
        else {
            loadMore=NO;
        }
    }];
}

@end
