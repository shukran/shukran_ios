//
//  AppDelegate.m
//  Shukran
//
//  Created by Venkatesh Badya on 22/09/16.
//  Copyright © 2016 The Lean Apps. All rights reserved.
//

#import "AppDelegate.h"
#import <CoreData/CoreData.h>
#import "OfferScreenVC.h"
#import "AppHelper.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>

@interface AppDelegate ()
{
    //Storyboard Object
    UIStoryboard *storyboard;
}

@end

@implementation AppDelegate
@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

+(AppDelegate *)getAppDelegate
{
    return (AppDelegate*)[UIApplication sharedApplication].delegate;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    [self checkForLanguageToSetNavigationDirection];
  
    if([AppHelper userDefaultsForKey:@"LanguageSelected"] == nil) {
        NSString * deviceLanguage = [[NSLocale preferredLanguages] objectAtIndex:0];
        if([deviceLanguage isEqualToString:@"ar"] || [deviceLanguage isEqualToString:@"ar-US"] || [deviceLanguage isEqualToString:@"ar-DE"]) {
            [AppHelper saveToUserDefaults:@"Arabic" withKey:@"Language"];
        }
        else {
            [AppHelper saveToUserDefaults:@"English" withKey:@"Language"];
        }
    }
    else {
        // Navigation to OfferScreen
        storyboard = [UIStoryboard storyboardWithName:@"StoryboardEnglish" bundle:nil];
        UIViewController *vc2 = [storyboard instantiateViewControllerWithIdentifier:@"OfferScreenVC"];
        NSArray *controllers = @[vc2];
        UINavigationController *navController = (UINavigationController *)self.window.rootViewController;
        [navController setViewControllers:controllers];
    }
    
    [Fabric with:@[[Crashlytics class]]];
    return YES;
}

// Checking for language to set navigation
-(void)checkForLanguageToSetNavigationDirection
{
    if([[AppHelper userDefaultsForKey:@"Language"] isEqualToString:@"Arabic"]) {
        if(([[NSProcessInfo processInfo] respondsToSelector:@selector(isOperatingSystemAtLeastVersion:)]) && [[NSProcessInfo processInfo] isOperatingSystemAtLeastVersion:(NSOperatingSystemVersion){9, 0, 0}]) {
            [[UIView appearance] setSemanticContentAttribute:UISemanticContentAttributeForceRightToLeft];
        }
    }
    else {
        if(([[NSProcessInfo processInfo] respondsToSelector:@selector(isOperatingSystemAtLeastVersion:)]) && [[NSProcessInfo processInfo] isOperatingSystemAtLeastVersion:(NSOperatingSystemVersion){9, 0, 0}]) {
            [[UIView appearance] setSemanticContentAttribute:UISemanticContentAttributeForceLeftToRight];
        }
    }
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}


#pragma mark - Core Data stack

//Creating Managed Object Context
- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil)
        return _managedObjectContext;
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];  // Creating Store Coordinator
    if (coordinator != nil)
    {
        _managedObjectContext = [[NSManagedObjectContext alloc] init]; // Creating Managed Onject Context
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

//Object Model
- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil)
        return _managedObjectModel;
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Shukran" withExtension:@"momd"];  // Getting the model url
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL]; // Initializing the Object model with model url
    return _managedObjectModel;
}

//Persistent Store Coordinator
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil)
        return _persistentStoreCoordinator;
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"Shukran"]; // Getting the store url
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) // Initializing the Store Coordinator with store url
        abort();
    
    return _persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject]; // Fetching the details available from Document Directory
}

#pragma mark - Core Data Saving support

- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil)
    {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error])
            abort();
    }
}

@end
