//
//  AppDelegate.h
//  Shukran
//
//  Created by Venkatesh Badya on 22/09/16.
//  Copyright © 2016 The Lean Apps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext; // Object Context
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel; // Object Model
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator; // Store Coordinator

- (void)saveContext;
+(AppDelegate *)getAppDelegate; // Class method
-(void)checkForLanguageToSetNavigationDirection;
@end

