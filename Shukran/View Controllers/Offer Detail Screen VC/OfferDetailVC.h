//
//  OfferDetailVC.h
//  Shukran
//
//  Created by Venkatesh Badya on 22/09/16.
//  Copyright © 2016 The Lean Apps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OfferDetailVC : UIViewController

@property (strong,nonatomic) NSMutableDictionary * offerDetailsDictionary;
@property (strong,nonatomic) NSMutableArray * latestOfferArray;
@property (strong,nonatomic) NSIndexPath * selectedIndex;
@end
