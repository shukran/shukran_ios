//
//  OfferDetailVC.m
//  Shukran
//
//  Created by Venkatesh Badya on 22/09/16.
//  Copyright © 2016 The Lean Apps. All rights reserved.
//

#import "OfferDetailVC.h"
#import "Define.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "AppDelegate.h"
#import "AppHelper.h"

@interface OfferDetailVC () <UIScrollViewDelegate, UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
{
    // Objects for NSLayoutConstraint and NSDateFormatter
    NSLayoutConstraint * descriptionLabelHeightConstant;
    NSLayoutConstraint * descriptionViewHeightConstant;
    NSLayoutConstraint * titleLabelHeightConstant;
    NSLayoutConstraint * containerViewHeightConstant;
    
    NSDateFormatter *dateFormatter;
    UICollectionViewFlowLayout * floawlayout;
}
//Outlets for TableView and Label
@property (weak, nonatomic) IBOutlet UITableView *offerDetailTableView;
@property (weak, nonatomic) IBOutlet UILabel *hiddenDescriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *hiddenTitleLabel;

@end

@implementation OfferDetailVC
@synthesize offerDetailsDictionary,latestOfferArray,selectedIndex;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if([[AppHelper userDefaultsForKey:@"Language"] isEqualToString:@"English"]){
        [[UIView appearance] setSemanticContentAttribute:UISemanticContentAttributeForceLeftToRight];
    }
    else{
        [[UIView appearance] setSemanticContentAttribute:UISemanticContentAttributeForceRightToLeft];
    }
    
    // Creation of flow layout
    floawlayout = [[UICollectionViewFlowLayout alloc] init];
    [floawlayout setItemSize:CGSizeMake(150, 150)];
    [floawlayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    floawlayout.minimumLineSpacing = 10;
    floawlayout.minimumInteritemSpacing = 25;
    
    //Calculation of label height
    [self calculateLabelHeight];
        
    //Avoiding unnecessary scrolling on TableView and Reloading
    [self.offerDetailTableView reloadData];
    self.offerDetailTableView.alwaysBounceVertical = NO;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - StatusBar

//Changing StatusBar color
- (UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

#pragma mark - Calculation function

//Calculation of label height aacording to text
- (float)sizeForLabel:(UILabel *)descriptionLabel{
    
    //Calculation width of screen in which label text needs to be fixed, keeping height constant
    CGSize constrainedSize = CGSizeMake(self.view.frame.size.width - LEFTSPACING - RIGHTSPACING , MAXHEIGHT);
    
    //Fetching labelFontName and labelFontSize from label
    NSString *labelFontName = descriptionLabel.font.fontName;
    CGFloat labelFontSize = descriptionLabel.font.pointSize;
    
    //Creating attributed dictionary according to label attributes
    NSDictionary *attributesDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                          [UIFont fontWithName:labelFontName size:labelFontSize], NSFontAttributeName,
                                          nil];
    
    //Fetching text in the label for which height needs to be calculated
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:descriptionLabel.text attributes:attributesDictionary];
    //Calculation of new frame of label according to text with given constraints and attributes
    CGRect requiredLabelHeight = [string boundingRectWithSize:constrainedSize options:NSStringDrawingUsesLineFragmentOrigin context:nil];
    
    //Setting new frame by changing height value to label(whose height needs to be calculated)
    CGRect newLabelFrame = descriptionLabel.frame;
    newLabelFrame.size.height = requiredLabelHeight.size.height;
    
    // Returning new height of label
    return newLabelFrame.size.height +10;
}

-(void) calculateLabelHeight
{
    // Setting text to HiddenDescriptionLabel from OfferDetailsDictionary
    self.hiddenDescriptionLabel.numberOfLines = 0;
    if([offerDetailsDictionary objectForKey:@"LongDescription"]) {
        self.hiddenDescriptionLabel.text = [offerDetailsDictionary objectForKey:@"LongDescription"];
    }
    else {
        self.hiddenDescriptionLabel.text = @"";
    }
    
    // Setting text to HiddenTitleLabel from OfferDetailsDictionary
    self.hiddenTitleLabel.numberOfLines = 0;
    if([offerDetailsDictionary objectForKey:@"OfferTitle"]) {
        self.hiddenTitleLabel.text = [offerDetailsDictionary objectForKey:@"OfferTitle"];
    }
    else {
        self.hiddenTitleLabel.text = @"";
    }
}

#pragma mark - Button Actions

//Cross Button Action
- (IBAction)crossButtonAction:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UITableView Delegates and DataSources

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell * offerDetailCell;
    if(offerDetailCell == nil) {
        offerDetailCell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"OfferDetailCell" forIndexPath:indexPath];
    }
    
    //Setting layer of ValidityView
    UIView * validityView = (UIView *)[offerDetailCell.contentView viewWithTag:107];
    validityView.layer.cornerRadius = 3.0;
    validityView.layer.masksToBounds = YES;
    
    // Fetching ImageView, UIView and Label from tag
    UIView * descriptionView = (UIView *) [offerDetailCell.contentView viewWithTag:102];
    UILabel * offerTitleLabel = (UILabel *) [offerDetailCell.contentView viewWithTag:104];
    __weak UIImageView * brandImageView = (UIImageView *) [offerDetailCell.contentView viewWithTag:103];
    UILabel * offerDescriptionLabel = (UILabel *) [offerDetailCell.contentView viewWithTag:105];
    UILabel * offerValidityLabel = (UILabel *) [offerDetailCell.contentView viewWithTag:106];
    
    UICollectionView * latestOfferCollectionView = (UICollectionView *)[offerDetailCell.contentView viewWithTag:205];
    [latestOfferCollectionView setCollectionViewLayout:floawlayout];
    
    // ContainerView to hold OfferDescription, OfferImage and OfferValidity
    UIView * containerView = (UIView *) [offerDetailCell.contentView viewWithTag:301];
    containerView.layer.cornerRadius = 3.0;
    containerView.layer.borderWidth = 1.0;
    containerView.layer.masksToBounds = YES;
    containerView.layer.borderColor = [UIColor colorWithRed:230.0/255.0 green:230.0/255.0 blue:230.0/255.0 alpha:1.0].CGColor;
    
    //Fetching constraints from OfferDescriptionLabel, OfferTitleLabel and OfferDescriptionView
    NSArray * offerDescriptionLabelConstraints = [offerDescriptionLabel constraints];
    NSArray * offerDescriptionViewConstraints = [descriptionView constraints];
    NSArray * offerTitleLabelConstraints = [offerTitleLabel constraints];
    NSArray * offerBackgroundViewConstraints = [containerView constraints];
    
    // Fetching constraints from array index according to iOS version
    if (SYSTEM_VERSION_LESS_THAN(@"10.0")) {
        if(offerDescriptionLabelConstraints.count > 1) {
            descriptionLabelHeightConstant = [offerDescriptionLabelConstraints objectAtIndex:2];
            titleLabelHeightConstant = [offerTitleLabelConstraints objectAtIndex:2];
        }
        else {
            descriptionLabelHeightConstant = [offerDescriptionLabelConstraints objectAtIndex:0];
            titleLabelHeightConstant = [offerTitleLabelConstraints objectAtIndex:0];
        }
    }
    else {
        if(offerDescriptionLabelConstraints.count > 1) {
            descriptionLabelHeightConstant = [offerDescriptionLabelConstraints objectAtIndex:2];
            titleLabelHeightConstant = [offerTitleLabelConstraints objectAtIndex:2];
        }
        else {
            descriptionLabelHeightConstant = [offerDescriptionLabelConstraints objectAtIndex:0];
            titleLabelHeightConstant = [offerTitleLabelConstraints objectAtIndex:0];
        }
    }
    
    containerViewHeightConstant = [offerBackgroundViewConstraints objectAtIndex:0];
    descriptionViewHeightConstant = [offerDescriptionViewConstraints objectAtIndex:0];
    
    // Setting image to OfferImage
    __weak UIImageView * offerImageView = (UIImageView *) [offerDetailCell.contentView viewWithTag:101];
    NSURL * offerImageUrl;
    if(![[offerDetailsDictionary objectForKey:@"ImageName"]isEqualToString:@""]) {
        offerImageUrl = [NSURL URLWithString:[offerDetailsDictionary objectForKey:@"ImageName"]];
        [offerImageView sd_setImageWithURL:offerImageUrl placeholderImage:[UIImage imageNamed:@"Offer Placeholder Image"]];
    }
    else {
        offerImageUrl = [NSURL URLWithString:@""];
        if([[AppHelper userDefaultsForKey:@"Language"] isEqualToString:@"Arabic"]) {
            offerImageView.image = [UIImage imageNamed:@"No Offer Image Arabic"];
        }
        else {
            offerImageView.image = [UIImage imageNamed:@"No Offer Image English"];
        }
    }
    
    // Setting image to BrandImage
    NSURL * brandImageUrl;
    if(![[offerDetailsDictionary objectForKey:@"BrandImage"]isEqualToString:@""]) {
        brandImageUrl = [NSURL URLWithString:[offerDetailsDictionary objectForKey:@"BrandImage"]];
        [brandImageView sd_setImageWithURL:brandImageUrl placeholderImage:[UIImage imageNamed:@"Brand Placeholder Image"]];
    }
    else {
        brandImageUrl = [NSURL URLWithString:@""];
        brandImageView.image = [UIImage imageNamed:@"No Brand Image"];
    }
    
    //Setting text to OfferTtitleLabel and making numberOfLiines to 0 (to make label height dynamic)
    offerTitleLabel.numberOfLines = 0;
    if([offerDetailsDictionary objectForKey:@"OfferTitle"]) {
        offerTitleLabel.text = [offerDetailsDictionary objectForKey:@"OfferTitle"];
    }
    else {
        offerTitleLabel.text = @"";
    }
    
    //Setting text to ValidityLabel
    if([offerDetailsDictionary objectForKey:@"OfferValidDate"]) {
        
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
        if([[AppHelper userDefaultsForKey:@"Language"] isEqualToString:@"Arabic"]) {
            [dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"ar"]];
        }
        else {
            [dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
        }
        NSDate * offerDate = [dateFormatter dateFromString:[offerDetailsDictionary objectForKey:@"OfferValidDate"]];
        
        // Convert date object into desired format
        [dateFormatter setDateFormat:@"dd MMM yyyy"];
        NSString * convertedDateFormat = [dateFormatter stringFromDate:offerDate];
        
        offerValidityLabel.text = [NSString stringWithFormat:@"%@ %@",NSLocalizedStringFromTable(@"Valid until:", [AppHelper userDefaultsForKey:@"Language"], nil),convertedDateFormat];
    }
    else {
        offerValidityLabel.text = @"";
    }
    
    // Setting text to OfferDescriptionLabel from OfferDetailsDictionary and making numberOfLines to 0 (to make label height dynamic)
    offerDescriptionLabel.numberOfLines = 0;
    if([offerDetailsDictionary objectForKey:@"LongDescription"]) {
        offerDescriptionLabel.text = [offerDetailsDictionary objectForKey:@"LongDescription"];
    }
    else {
        offerDescriptionLabel.text = @"";
    }
    
    //Calling function to dynamically calculate the height of OfferDescription label and OfferTitle label
    float descriptionLabelSizeHeight = [self sizeForLabel:offerDescriptionLabel];
    float titleLabelSizeHeight = [self sizeForLabel:offerTitleLabel];
    
    //Adjusting NSLayoutConstraints of OfferDescriptionLabel and OfferDescriptionView according to height of OfferDescriptionLabel
    descriptionLabelHeightConstant.constant = descriptionLabelSizeHeight;
    titleLabelHeightConstant.constant = titleLabelSizeHeight;
    descriptionViewHeightConstant.constant = descriptionLabelSizeHeight + titleLabelSizeHeight + DESCRIPTIONVIEWHEIGHT;
    containerViewHeightConstant.constant = descriptionViewHeightConstant.constant + CONTAINERVIEWSIZE;
    
    //Disabling cell selection style
    offerDetailCell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    [latestOfferCollectionView reloadData];
    
    return offerDetailCell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    //Calculating label height and adding fixed cell cell (deciding whether to display latest offer section)
    return [self sizeForLabel:self.hiddenDescriptionLabel] + [self sizeForLabel:self.hiddenTitleLabel] + CELLHEIGHTWITHCOLLECTIONVIEW;
}

#pragma mark - UICollectionView DataSources and Delegates

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return latestOfferArray.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell * latestOfferCell;
    if(latestOfferCell == nil) {
        latestOfferCell = (UICollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"LatestOfferCell" forIndexPath:indexPath];
    }
    
    latestOfferCell.layer.cornerRadius = 2.0;
    latestOfferCell.layer.borderWidth = 2.0;
    latestOfferCell.layer.masksToBounds = YES;
    latestOfferCell.layer.borderColor = [UIColor colorWithRed:230.0/255.0 green:230.0/255.0 blue:230.0/255.0 alpha:1.0].CGColor;
    
    UIView * containerView = (UIView *)[latestOfferCell.contentView viewWithTag:200];
    UIImageView * latestBrandImage = (UIImageView *)[latestOfferCell.contentView viewWithTag:201];
    UILabel * latestOfferLabel = (UILabel *)[latestOfferCell.contentView viewWithTag:202];
    UILabel * otherOfferLabel = (UILabel *)[self.view viewWithTag:203];
    
    //Setting layer of ContainerView to hold Images and OfferDescription
    containerView.layer.cornerRadius = 3.0;
    containerView.layer.borderWidth = 1.0;
    containerView.layer.masksToBounds = YES;
    containerView.layer.borderColor = [UIColor colorWithRed:230.0/255.0 green:230.0/255.0 blue:230.0/255.0 alpha:1.0].CGColor;
    
    // Setting image to LatestBrandImage
    NSURL * brandImageUrl;
    if(![[[latestOfferArray objectAtIndex:indexPath.item] objectForKey:@"BrandImage"]isEqualToString:@""]) {
        brandImageUrl = [NSURL URLWithString:[[latestOfferArray objectAtIndex:indexPath.item] objectForKey:@"BrandImage"]];
        [latestBrandImage sd_setImageWithURL:brandImageUrl placeholderImage:[UIImage imageNamed:@"Brand Placeholder Image"]];
    }
    else {
        brandImageUrl = [NSURL URLWithString:@""];
        latestBrandImage.image = [UIImage imageNamed:@"No Brand Image"];
    }
    
    //Seeting text for OtherOfferLabel
    otherOfferLabel.text = NSLocalizedStringFromTable(@"OTHER OFFERS", [AppHelper userDefaultsForKey:@"Language"], nil);
    
    // Setting image to LatestOfferLabel
    latestOfferLabel.text = [[latestOfferArray objectAtIndex:indexPath.item] objectForKey:@"SortDescription"];
    
    return latestOfferCell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    offerDetailsDictionary = [latestOfferArray objectAtIndex:indexPath.item];
    
    //Calculation of label height
    [self calculateLabelHeight];
    
    //Avoiding unnecessary scrolling on TableView and Reloading
    [self.offerDetailTableView reloadData];
    self.offerDetailTableView.alwaysBounceVertical = NO;
}


@end
