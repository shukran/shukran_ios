//
//  DatabaseUtility.m
//  Infinea
//
//  Created by Venkatesh on 5/6/15.
//  Copyright (c) 2015 Ritu Dalal. All rights reserved.
//

#import "DatabaseUtility.h"
#import "AppDelegate.h"
#import "Define.h"
#import "Offer_Details_En+CoreDataClass.h"
#import "Offer_Details_En+CoreDataProperties.h"
#import "AppDelegate.h"

@implementation DatabaseUtility

@synthesize _managedObjectContext=managedObjectContext,_managedObject = managedObject;
-(id) init
{
    self=[super init];
    if(self)
    {
        self._managedObjectContext = [AppDelegate getAppDelegate].managedObjectContext;
    }
    return self;
}
+(DatabaseUtility *) getUtility
{
    static DatabaseUtility *singleton=nil;
    if(!singleton)
    {
        singleton=[[DatabaseUtility alloc]init];
    }
    return singleton;
}
#pragma mark - Core Data Operation
#pragma mark -
-(NSEntityDescription *) getEntityDescription:(NSString *)tableName
{
    NSEntityDescription *entity = [NSEntityDescription entityForName:tableName inManagedObjectContext:self._managedObjectContext];
    return entity;
}

-(BOOL)insertDataInTable:(NSString *)tableName withDictionary:(NSMutableDictionary *)offerDictionary
{
    Offer_Details_En * offerDetailsObj = [[Offer_Details_En alloc] initWithEntity:[self getEntityDescription:tableName] insertIntoManagedObjectContext:self._managedObjectContext];
    
    if([[[NSUserDefaults standardUserDefaults]valueForKey:@"Language"] isEqualToString:@"English"]) {
        [offerDetailsObj setValue:[offerDictionary objectForKey:@"BrandImage"] forKey:@"brandImage_en"];
        [offerDetailsObj setValue:[offerDictionary objectForKey:@"ImageName"] forKey:@"imageName_en"];
        [offerDetailsObj setValue:[offerDictionary objectForKey:@"LanguageCode"] forKey:@"languageCode_en"];
        [offerDetailsObj setValue:[offerDictionary objectForKey:@"LongDescription"] forKey:@"longDescription_en"];
        [offerDetailsObj setValue:[offerDictionary objectForKey:@"OfferId"] forKey:@"offerId_en"];
        [offerDetailsObj setValue:[offerDictionary objectForKey:@"OfferThumbImage"] forKey:@"offerThumbImage_en"];
        [offerDetailsObj setValue:[offerDictionary objectForKey:@"OfferTitle"] forKey:@"offerTitle_en"];
        [offerDetailsObj setValue:[offerDictionary objectForKey:@"OfferValidDate"] forKey:@"offerValidDate_en"];
        [offerDetailsObj setValue:[offerDictionary objectForKey:@"SortDescription"] forKey:@"shortDescription_en"];
        [offerDetailsObj setValue:[offerDictionary objectForKey:@"Privilege"] forKey:@"offerPrivilege"];
        [offerDetailsObj setValue:[offerDictionary objectForKey:@"Created"] forKey:@"offerCreatedDate_en"];
    }
    else {
        [offerDetailsObj setValue:[offerDictionary objectForKey:@"BrandImage"] forKey:@"brandImage_ar"];
        [offerDetailsObj setValue:[offerDictionary objectForKey:@"ImageName"] forKey:@"imageName_ar"];
        [offerDetailsObj setValue:[offerDictionary objectForKey:@"LanguageCode"] forKey:@"languageCode_ar"];
        [offerDetailsObj setValue:[offerDictionary objectForKey:@"LongDescription"] forKey:@"longDescription_ar"];
        [offerDetailsObj setValue:[offerDictionary objectForKey:@"OfferId"] forKey:@"offerId_ar"];
        [offerDetailsObj setValue:[offerDictionary objectForKey:@"OfferThumbImage"] forKey:@"offerThumbImage_ar"];
        [offerDetailsObj setValue:[offerDictionary objectForKey:@"OfferTitle"] forKey:@"offerTitle_ar"];
        [offerDetailsObj setValue:[offerDictionary objectForKey:@"OfferValidDate"] forKey:@"offerValidDate_ar"];
        [offerDetailsObj setValue:[offerDictionary objectForKey:@"SortDescription"] forKey:@"shortDescription_ar"];
        [offerDetailsObj setValue:[offerDictionary objectForKey:@"Privilege"] forKey:@"offerPrivilege"];
        [offerDetailsObj setValue:[offerDictionary objectForKey:@"Created"] forKey:@"offerCreatedDate_ar"];
    }
    
    self._managedObject = offerDetailsObj;
    NSError *error=nil;
    if (![self._managedObjectContext save:&error])
        return FALSE;
    return TRUE;
}

-(BOOL) isDataAvailableInTable:(NSString *) tableName forPredicate:(NSPredicate *)predicate
{
    // initializing NSFetchRequest
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    if(predicate!=nil)
        fetchRequest.predicate=predicate;
    
    //Setting Entity to be Queried
    NSEntityDescription *entity = [self getEntityDescription:tableName];
    [fetchRequest setEntity:entity];
    NSError* error;
    
    // Query on managedObjectContext With Generated fetchRequest
    NSArray *fetchedRecords = [self._managedObjectContext executeFetchRequest:fetchRequest error:&error];
    if(fetchedRecords.count>0)
    {
        self._managedObject = [fetchedRecords objectAtIndex:0];
        return TRUE;
    }
    else
        return FALSE;
}
-(NSArray *) getAllDataFromTable:(NSString *) tableName forPredicate:(NSPredicate *) predicate withDescriptor:(NSArray *)sortDescriptors
{
    // initializing NSFetchRequest
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setReturnsObjectsAsFaults:NO];
    if(predicate!=nil)
        fetchRequest.predicate=predicate;
    
    if(sortDescriptors!=nil)
        [fetchRequest setSortDescriptors:sortDescriptors];
    
    //Setting Entity to be Queried
    NSEntityDescription *entity = [self getEntityDescription:tableName];
    [fetchRequest setEntity:entity];
    NSError* error;
    
    // Query on managedObjectContext With Generated fetchRequest
    NSArray *fetchedRecords = [self._managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    // Returning Fetched Records
    return fetchedRecords;
}

-(BOOL) deleteRecordsFromTable:(NSString *)tableName withRecord:(NSManagedObject *)managedObject1
{
    NSError* error;
    [self._managedObjectContext deleteObject:managedObject1];
    if (![self._managedObjectContext save:&error])
        return FALSE;
    return TRUE;
}

+(BOOL) isBlank:(NSString *)data
{
    if(data==nil || [data isEqualToString:@""])
        return TRUE;
    
    return FALSE;
}
+(void) alertViewTitle:(NSString *)title withMessage:(NSString *)message
{
    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:title message:message delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    [alert show];
    
}
+(BOOL) IsValidEmail:(NSString *)emailString Strict:(BOOL)strictFilter
{
    NSString *stricterFilterString = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSString *laxString = @".+@.+\\.[A-Za-z]{2}[A-Za-z]*";
    
    NSString *emailRegex = strictFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:emailString];
}
@end
