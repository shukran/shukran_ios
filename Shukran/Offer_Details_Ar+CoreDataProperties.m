//
//  Offer_Details_Ar+CoreDataProperties.m
//  
//
//  Created by Venkatesh Badya on 29/09/16.
//
//

#import "Offer_Details_Ar+CoreDataProperties.h"

@implementation Offer_Details_Ar (CoreDataProperties)

+ (NSFetchRequest<Offer_Details_Ar *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Offer_Details_Ar"];
}

@dynamic brandImage_ar;
@dynamic imageName_ar;
@dynamic languageCode_ar;
@dynamic longDescription_ar;
@dynamic offerId_ar;
@dynamic offerThumbImage_ar;
@dynamic offerTitle_ar;
@dynamic offerValidDate_ar;
@dynamic shortDescription_ar;
@dynamic offerCreatedDate_ar;
@dynamic offerPrivilege;

@end
