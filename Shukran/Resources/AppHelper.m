//
//  AppHelper.m
//  Infinea
//
//  Created by Ritu Dalal on 6/26/14.
//  Copyright (c) 2014 Ritu Dalal. All rights reserved.
//

#import "AppHelper.h"
//djbhsjkdfhjshdf
@implementation AppHelper

+(void)saveToUserDefaults:(id)value withKey:(NSString*)key
{
	NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
	if (standardUserDefaults) {
		[standardUserDefaults setObject:value forKey:key];
		[standardUserDefaults synchronize];
	}
}

+(NSString*)userDefaultsForKey:(NSString*)key
{
	NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
	NSString *val = nil;
    
	if (standardUserDefaults)
		val = [standardUserDefaults objectForKey:key];
	
	return val;
}
+(NSArray*)userDefaultsForArray:(NSString*)key
{
	NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
	NSArray *val = nil;
	
	if (standardUserDefaults)
		val = [standardUserDefaults objectForKey:key];
	
	return val;
}
+(void)removeFromUserDefaultsWithKey:(NSString*)key
{
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    
    [standardUserDefaults removeObjectForKey:key];
    [standardUserDefaults synchronize];
}


+ (void) showAlertViewWithTag:(NSInteger)tag title:(NSString*)title message:(NSString*)msg delegate:(id)delegate cancelButtonTitle:(NSString*)CbtnTitle otherButtonTitles:(NSString*)otherBtnTitles
{
    if(CbtnTitle !=nil)
    {
        if([CbtnTitle isEqualToString:@"No"])
        {
            CbtnTitle=NSLocalizedStringFromTable(@"No", [AppHelper getCurrentLanguage], nil);
        }
        else if([CbtnTitle isEqualToString:@"Yes"])
        {
            CbtnTitle=NSLocalizedStringFromTable(@"Yes", [AppHelper getCurrentLanguage], nil);
        }
    }
    if(otherBtnTitles !=nil)
    {
        if([otherBtnTitles isEqualToString:@"OK"])
        {
            otherBtnTitles=NSLocalizedStringFromTable(@"OK", [AppHelper getCurrentLanguage], nil);
        }
        else if([otherBtnTitles isEqualToString:@"No"])
        {
            otherBtnTitles=NSLocalizedStringFromTable(@"No", [AppHelper getCurrentLanguage], nil);
        }
        else if([otherBtnTitles isEqualToString:@"Yes"])
        {
            otherBtnTitles=NSLocalizedStringFromTable(@"Yes", [AppHelper getCurrentLanguage], nil);
        }
    }
}



+ (NSString *)getCurrentLanguage
{
    return @"English";
}

+(NSString *)convertDotToComma:(NSString *)dotString
{
    NSString * commaString = [dotString stringByReplacingOccurrencesOfString:@"." withString:@","];
    return commaString;
}

@end
