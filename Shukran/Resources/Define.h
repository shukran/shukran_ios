//
//  Define.h
//  Shukran
//
//  Created by Venkatesh Badya on 24/09/16.
//  Copyright © 2016 The Lean Apps. All rights reserved.
//

#ifndef Define_h
#define Define_h

#define LEFTSPACING                                        32
#define RIGHTSPACING                                       32
#define MAXHEIGHT                                          9999
#define CELLHEIGHTWITHCOLLECTIONVIEW                       555
#define DESCRIPTIONVIEWHEIGHT                              55
#define CELLHEIGHTWITHOUTCOLLECTIONVIEW                    345
#define OFFERLISTCELLSIZE                                  250
#define CONTAINERVIEWSIZE                                  220
#define BASEURL                                            @"http://52.66.158.119/"
#define Notification_Offer_List                            @"Notification_Offer_List"
#define SYSTEM_VERSION_LESS_THAN(v)                        ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)

#endif
